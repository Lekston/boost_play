#include <atomic>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <thread>
#include <vector>

#include "utils/term_format.hpp"

// backtrace
#include "backtrace.h"
#include "globals.h"
#include "multithread_stack.h"

#include "boost_base64.hpp"
#include "boost_serialization.hpp"
#include "bytes.hpp"
#include "parser_proxy.hpp"

#include "utils/tictoc.hpp"

/*****************************************************************************/

template<class T_IN, class T_OUT> void serialization_tests();
template<class T_IN, class T_OUT> void serialization_to_string();

void base64_tests();
void print_fdm(struct SITL::sitl_fdm& fdm);

/*****************************************************************************/

enum BT_LOAD {
    none = 0,
    once = 1,
    alot = 2
};

std::atomic<enum BT_LOAD>  backtrace_load = BT_LOAD::none;
std::mutex mtx;
std::atomic<int> some_number{0};

void test_function(std::string str = "anonymous")
{
    //std::this_thread::sleep_for(std::chrono::milliseconds(50));
    std::unique_lock<std::mutex> lock{mtx};
    std::cout << fmt::orange_bg << "ENTER to " << str << fmt::reset << std::endl;
    //std::cout << "boost:\n" << std::string(BOOST_CURRENT_FUNCTION) << std::endl;

    SITL::sitl_fdm fdm = { 100, 50.0, 20.0, 250.0,  180.0, 1.0, 2.0, 3.0 };

    fdm.timestamp_us = 1000 * ++some_number;
    double res = 250 + some_number;
    fdm.altitude = res;

    if (backtrace_load == BT_LOAD::once)
    {
        std::string bt_str = fast_backtrace();
        std::cout << "fast_bt:\n" << bt_str << std::endl;
        RECORDERS_REC_PLAY(fdm, bt_str);
        res = 2.05*res + std::sqrt((float)(res));
        fdm.altitude = res;
        RECORDERS_REC_CHECK(fdm, bt_str);
    }
    else if (backtrace_load == BT_LOAD::alot)
    {
        std::cout << fmt::red << "PERF TESTING" << fmt::reset << std::endl;
        for (int i=0; i<100; i++) {
            //std::cout << "sys_bt:\n" << sys_backtrace() << std::endl;
            std::string sub{"__invoke_impl"};
            std::string bt_str = fast_backtrace();
            std::string bt_str_ref = "";//unw_backtrace();
            size_t found = bt_str_ref.find(sub);
            if (found != std::string::npos) {
                std::cout << "unw_bt:\n" << bt_str_ref.substr(0, found + sub.length()) << std::endl;
            } else {
                std::cout << "unw_bt:\n" << bt_str_ref << std::endl;
            }
            std::cout << "fast_bt:\n" << bt_str << std::endl;
        }
    }

    std::cout << "Res: " << res << std::endl;
    std::cout << fmt::orange_bg << "EXIT from " << str << fmt::reset << std::endl;
}

void test_func_wrap(std::vector<std::string>& vec)
{
    std::string str{"wrap_"};
    str += vec.empty() ? "" : (*vec.begin());
    test_function(str);
}

class TestClass
{
public:
    TestClass()
    {
        _this_cnt = _total_cnt++;
    }

    void func() {
        //std::this_thread::sleep_for(std::chrono::milliseconds(50));
        test_function("method from obj #" + std::to_string(_this_cnt));
    }

private:
    static std::atomic<int> _total_cnt;

    int _this_cnt;
};
std::atomic<int> TestClass::_total_cnt{0};

void test_backtrace(enum BT_LOAD load = BT_LOAD::none)
{
    backtrace_load = load;

    TestClass obj0{};
    TestClass obj1{};

    std::vector<std::thread> vthr;
    vthr.reserve(8);

    vthr.emplace_back( std::thread{std::bind(test_function, "thread_0")} );
    vthr.emplace_back( std::thread{std::bind(test_function, "thread_1")} );

    vthr.emplace_back( std::thread{&TestClass::func, obj0} );
    vthr.emplace_back( std::thread{&TestClass::func, obj1} );

    vthr.emplace_back( std::thread{std::bind(test_func_wrap, std::vector<std::string>{"thread_0"})} );
    vthr.emplace_back( std::thread{std::bind(test_func_wrap, std::vector<std::string>{"thread_1"})} );

    test_function("main1");
    test_function("main2");

    for (auto& t : vthr) {
        if (t.joinable())
            t.join();
    }
}

int main(int ac, char** av)
{
    pthread_mutex_init(&glob_instr_func_mtx, NULL);
    pthread_mutex_lock(&glob_instr_func_mtx);
    glob_multiStack_ptr = MapTID2Stack_new();

    // TODO use C-style atomic bool
    glob_within_main = true;

    pthread_mutex_unlock(&glob_instr_func_mtx);

    Recorders::RecPlayEngine::get_instance()->set_state(Recorders::RecPlayEngine::State::Recording);

    std::cout << "Signal Sniffer demo" << std::endl;

    // print_types();
    TicToc perf;

    auto run_bt_test = [&](enum BT_LOAD lvl){
        perf.tic();
        for (int i=0; i<1; i++)
            test_backtrace(lvl);
        std::cout << std::endl;
        std::cout << fmt::green_bg << "ELAPSED: " << perf.toc() << fmt::reset << std::endl;
        std::cout << std::endl;
    };

    try{
        // serialization_tests<BoostIBinArchive, BoostOBinArchive>();
        // serialization_tests<BoostITxtArchive, BoostOTxtArchive>();
        // serialization_to_string<BoostIBinArchive, BoostOBinArchive>();
        // base64_tests();

        // run_bt_test(BT_LOAD::none);
        run_bt_test(BT_LOAD::once);
        // run_bt_test(BT_LOAD::alot);

        // TODO test cases with string::compare()
    } catch (boost::archive::archive_exception& ec) {
        std::cout << "Caught: Boost archive exception " << ec.what() << std::endl;
    } catch (std::exception& ec) {
        std::cout << "Caught: " << ec.what() << std::endl;
        std::cout << typeid(ec).name() << std::endl;
    }

    pthread_mutex_lock(&glob_instr_func_mtx);
    glob_within_main = false;
    MapTID2Stack_delete(glob_multiStack_ptr);
    pthread_mutex_unlock(&glob_instr_func_mtx);
    pthread_mutex_destroy(&glob_instr_func_mtx);

    return 0;
}

/*****************************************************************************/

template<class T_IN, class T_OUT>
void serialization_tests()
{
    SITL::sitl_fdm fdm00 = { 100, 50.0, 20.0, 250.0,  180.0, 1.0, 2.0, 3.0 };
    SITL::sitl_fdm fdm01 = { 199, 52.2, 20.0, 251.0,  177.0, 1.0, 2.0, 3.0 };
    SITL::sitl_fdm fdm02 = { 255, 58.8, 29.9, 300.0, -177.0, 1.0, 2.0, 3.0 };

    SITL::sitl_fdm fdm90 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    SITL::sitl_fdm fdm91 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    SITL::sitl_fdm fdm92 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };


    {
        std::ofstream fstr{"test_file.txt"};

        T_OUT trace_archive = T_OUT{fstr};

        trace_archive & fdm00;
        trace_archive & fdm01;
        trace_archive & fdm02;
    }

    std::cout << "Using: " << typeid(T_IN).name() << " & " << typeid(T_OUT).name() << std::endl;
    std::cout << "Before:" << std::endl;
    print_fdm(fdm00);
    print_fdm(fdm01);
    print_fdm(fdm02);

    {
        std::ifstream fstr{"test_file.txt"};

        T_IN trace_archive = T_IN{fstr};

        trace_archive & fdm90;
        trace_archive & fdm91;
        trace_archive & fdm92;
    }

    std::cout << "After:" << std::endl;
    print_fdm(fdm90);
    print_fdm(fdm91);
    print_fdm(fdm92);
}

template<class T_IN, class T_OUT>
void serialization_to_string()
{
    SITL::sitl_fdm fdm00 = { 100, 50.0, 20.0, 250.0,  180.0, 1.0, 2.0, 3.0 };
    SITL::sitl_fdm fdm01 = { 199, 52.2, 20.0, 251.0,  177.0, 1.0, 2.0, 3.0 };
    SITL::sitl_fdm fdm02 = { 255, 58.8, 29.9, 300.0, -177.0, 1.0, 2.0, 3.0 };

    SITL::sitl_fdm fdm90 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    SITL::sitl_fdm fdm91 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
    SITL::sitl_fdm fdm92 = { 0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

    std::stringstream ss;
    {
        T_OUT trace_archive{ss};

        trace_archive & fdm00;
        trace_archive & fdm01;
        trace_archive & fdm02;
    }

    std::cout << "Using: " << typeid(T_IN).name() << " & " << typeid(T_OUT).name() << std::endl;
    std::cout << "Before:" << std::endl;
    print_fdm(fdm00);
    print_fdm(fdm01);
    print_fdm(fdm02);

    // ENCODE
    auto enc = base64::encode_str(ss.str());

    std::cout << "binary data size: " << ss.str().size() << std::endl;
    std::cout << enc << std::endl;

    {
        std::ofstream ofs{"test_file.txt"};
        BoostOTxtArchive trace_archive{ofs};

        trace_archive & enc;
    }

    std::string dec_in;
    {
        std::ifstream ifs{"test_file.txt"};
        BoostITxtArchive trace_archive{ifs};

        trace_archive & dec_in;
    }

    // DECODE
    auto out = base64::decode_str(dec_in);

    std::cout << "base64 decoded to binary" << std::endl;
    std::cout << "binary data size: " << out.size() << std::endl;

    int cmp = ss.str().compare(0, out.size(), out);
    if (cmp == 0)
        std::cout << "binaries match" << std::endl;
    else
        std::cout << (int)(cmp) << std::endl;


    std::stringstream ss_out{out};
    {
        T_IN trace_archive{ss_out};

        trace_archive & fdm90;
        trace_archive & fdm91;
        trace_archive & fdm92;
    }

    std::cout << "After:" << std::endl;
    print_fdm(fdm90);
    print_fdm(fdm91);
    print_fdm(fdm92);
}


void base64_tests()
{
    SITL::sitl_fdm fdm[3] = {{ 100, 50.0, 20.0, 250.0,  180.0, 1.0, 2.0, 3.0 },
                             { 199, 52.2, 20.0, 251.0,  177.0, 1.0, 2.0, 3.0 },
                             { 255, 58.8, 29.9, 300.0, -177.0, 1.0, 2.0, 3.0 }};
    SITL::sitl_fdm fdm_out[3];

    std::cout << "BASE64 encode / decode tests:" << std::endl;
    for(int i=0; i<3; i++) {
        auto enc   = base64::encode_obj<SITL::sitl_fdm>(fdm[i]);
        fdm_out[i] = base64::decode_obj<SITL::sitl_fdm>(enc);

        print_fdm(fdm_out[i]);
    }
    // TODO make this into a unit test
}

void print_fdm(struct SITL::sitl_fdm& fdm)
{
    std::cout << "t:\t" << fdm.timestamp_us << std::setprecision(3)
              << " \t lat: " << fdm.latitude
              << " \t lon: " << fdm.longitude
              << " \t alt: " << fdm.altitude
              << " \t hdg: " << fdm.heading << std::endl;
}

