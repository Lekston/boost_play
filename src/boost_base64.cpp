#include "boost_base64.hpp"

namespace base64 {

size_t standarize_padding(std::string &str)
{
    // Standarize padding
    // If input (i.e. BASE64) size is not a multiple of 4, pad with =
    size_t missing_pad_chars{(4 - (str.size() % 4)) % 4};
    str.append(missing_pad_chars, '=');
    size_t pad_chars{0};
    for (auto iter=str.rbegin(); iter!=str.rend(); ++iter) {
        if (*iter == '=') {
            pad_chars++;
            *iter = 'A'; // set to 0 value (as per base64)
        }
        else break;
    }
    return pad_chars;
}

std::string encode_str(const std::string& str)
{
    std::string input{str.begin(), str.end()};
    std::string enc{base64_t{input.begin()}, base64_t{input.end()}};

    return enc;
}

std::string decode_str(std::string& str)
{
    size_t pad_chars = standarize_padding(str);
  #ifdef DEBUG_BOOST_BASE64
    std::cout << "Padding chars: " << pad_chars << std::endl;
  #endif // DEBUG_BOOST_BASE64

    std::string out{ binary_t{str.begin()},
                     binary_t{str.end()} };
    out.erase(out.end() - pad_chars, out.end());

    return out;
}

} // namespace base64