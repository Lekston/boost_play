#include "multithread_stack.h"

#include <unordered_map>
#include <stack>
#include <vector>

#ifdef __cplusplus
extern "C" {
#endif

struct StackImage : public std::vector<struct StackData>
{
    // Non-template name needed as a wrapper for C linkage
};

struct uMapTID2Stack : public std::unordered_map<uMapKey, StackImage>
{
    // Non-template name needed as a wrapper for C linkage
};

#define uMapKey_NOT_VALID 0
// #define uMapKey_NOT_VALID nullptr
// TODO can you do this with a template?? (is ptr??)

/*****************************************************************************/
/*
 * Wrapper for Unordered Map of StackImages
 */

uMapTID2Stack* MapTID2Stack_new()
{
    return new uMapTID2Stack{};
}

void MapTID2Stack_delete(uMapTID2Stack* obj)
{
    delete obj;
    obj = nullptr;
}

size_t MapTID2Stack_size(uMapTID2Stack* obj)
{
    return obj->size();
}

StackImage* MapTID2Stack_at(uMapTID2Stack* obj, uMapKey key)
{
    return &((*obj)[key]);
}

StackImage* MapTID2Stack_find(uMapTID2Stack* obj, uMapKey key)
{
    auto iter = obj->find(key);
    StackImage* img_ptr = nullptr;
    if (iter != obj->end()) {
        img_ptr = &(iter->second);
    }
    return img_ptr;
}

uMapKey MapTID2Stack_insert(uMapTID2Stack* obj, uMapKey req_key, StackImage* data)
{
    auto status_pair = obj->try_emplace(req_key, *data);
    uMapKey key = status_pair.first->first;
    if (!status_pair.second) {
        // Element exists
        key = uMapKey_NOT_VALID;
    }
    return key;
}

uMapKey MapTID2Stack_key_begin(uMapTID2Stack* obj)
{
    return obj->begin()->first;
}

uMapKey MapTID2Stack_key_next(uMapTID2Stack* obj, uMapKey prev_key)
{
    auto iter = obj->find(prev_key);
    uMapKey key = uMapKey_NOT_VALID;
    if (iter != obj->end() && ++iter != obj->end()) {
        key = iter->first;
    }
    return key;
}

/*****************************************************************************/
/*
 * Wrapper for Stack Image
 * 
 * Note: Implemented as vector to allow iteration
 */

StackImage* StackImage_new()
{
    return new StackImage{};
}

void StackImage_delete(StackImage* obj)
{
    delete obj;
    obj = nullptr;
}

size_t StackImage_size(StackImage* obj)
{
    return obj->size();
}

bool StackImage_empty(StackImage* obj)
{
    return obj->empty();
}

void StackImage_push_back(StackImage* obj, StackData* data)
{
    return obj->push_back(*data);
}

StackData* StackImage_peek_back(StackImage* obj)
{
    return &(*obj->rbegin());
}

void StackImage_pop_back(StackImage* obj)
{
    obj->pop_back();
}

StackData* StackImage_at_idx(StackImage* obj, size_t idx)
{
    return &((*obj)[idx]);
}

#ifdef __cplusplus
}
#endif