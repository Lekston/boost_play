#include "parser_engine.hpp"

namespace Recorders {

RecPlayEngine::RecPlayEngine(enum State state, std::string archive_path)
    : _state{state}
    , _archive_path{archive_path}
{
    // Nothing to do here
}

/*****************************************************************************/
// public methods
void
RecPlayEngine::set_state(State state)
{
    _state = state;
    this->_activate_archive();
}

void
RecPlayEngine::register_proxy( const std::string& datatype, ProxyCallback cb )
{
    _map_types[datatype] = cb;
}

/*****************************************************************************/
// public RecorderIface implementation methods
bool
RecPlayEngine::is_accepting(std::string& path) const
{
    return _state == State::Recording && !_inhibit_path(path);
}

bool
RecPlayEngine::is_scheduled_observation(std::string&) const
{
    // TODO
    return true; 
}

void
RecPlayEngine::record( const std::string& type,
                       const std::string& path,
                       const std::string& data )
{
    namespace ch = std::chrono;

    auto now = ch::steady_clock::now();
    auto dur_usec = ch::duration_cast<ch::microseconds>(now - ch::steady_clock::time_point{});
    uint64_t time = dur_usec.count();

    *_oarchive << time << type << path << data;
}

/*****************************************************************************/
// public PlayerIface implementation methods
bool
RecPlayEngine::is_publishing(std::string& path) const
{
    return (_state == State::Play_Master || _state == State::Play_Wait)
            && !_inhibit_path(path);
}

bool
RecPlayEngine::register_test_result(std::string& path, bool result)
{
    // TODO
    return true;
}

std::string
RecPlayEngine::get(const std::string& type, const std::string& path)
{
    std::string output;
    return {};
}


// Reads from an archive and distributes updates..
// Typically it is a simulation master, but it may need to wait for
// some tasks to complete.
void
RecPlayEngine::run()
{
    // TODO
    auto has_new = [](){ return true; };

    while (has_new()) {
        if (_do_exit)
            break;

        // auto req = _pending_requests.top()
        // _process_request(req);
        // _pending_requests.pop()

        // -> if no one already is waiting for the request -> sleep on a cv.wait_for (with timeout)
    }
    return;
}

/*****************************************************************************/
// private methods
void
RecPlayEngine::_activate_archive()
{
    if (_archive_path.empty())
        _archive_path = "record";

    _oarchive=nullptr;
    _iarchive=nullptr;

    switch (_state) {
    case State::Idle:
        break;
    case State::Recording:
        _ofstr = std::ofstream{_archive_path + "_out.txt"};
        _oarchive = std::make_shared<BoostOTxtArchive>(_ofstr);
        break;
    case State::Play_Master:
    case State::Play_Wait:
        _ifstr = std::ifstream{_archive_path + "_in.txt"};
        _iarchive = std::make_shared<BoostITxtArchive>(_ifstr);
        break;
    }
}

bool
RecPlayEngine::_inhibit_path(std::string& path) const
{
    // TODO: enable/disable iface for logging points
    return false;
}

} // namespace