// use pipes
#include <fcntl.h>
#include <unistd.h>

#include <iostream>

void stay_alive();

void pipe_tests()
{
    // 0 - reading end
    // 1 - writing end
    int pipe_fd[2];

    int dev_null = open("/dev/null", O_RDWR | O_CLOEXEC);
    if (pipe(pipe_fd) != 0) {
        std::cout << "Unable to create a pipe" << std::endl;
        exit(-1);
    }

    pid_t child_pid = fork();
    if (child_pid == 0) {
        close(pipe_fd[0]); // close reading-end as parent does not use it

        std::cout << "This is the child!" << std::endl;

        setsid();
        dup2(dev_null,   0); // redirect stdin to read from /dev/null
        dup2(pipe_fd[1], 1); // redirect stdout to write to pipe

        int ret = execlp("ls", "-al", nullptr);
        std::cout << "Finished with status: " << ret << std::endl;

        stay_alive();
        std::cout << "Child finished" << std::endl;
        return;
    } else {
        close(pipe_fd[1]); // close writing-end as parent does not use it

        std::cout << "This is the parent!" << std::endl;

        const int BUF_SIZE = 100;
        char buf[BUF_SIZE];

        read(pipe_fd[0], buf, BUF_SIZE);

        std::cout << "Parent received:\n" << std::string(buf) << std::endl;

        stay_alive();
        std::cout << "Parent finished" << std::endl;
    }
}

void stay_alive()
{
    char c;
    std::cin.getline(&c, 1);
}