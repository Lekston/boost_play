#include "parser_proxy.hpp"

typedef Recorders::RecPlayTypeProxy<int> ProxyInt;
typedef Recorders::RecPlayTypeProxy<float> ProxyFloat;
typedef Recorders::RecPlayTypeProxy<double> ProxyDouble;

template<> std::once_flag Singleton<ProxyInt>::m_flag;
template<> std::once_flag Singleton<ProxyFloat>::m_flag;
template<> std::once_flag Singleton<ProxyDouble>::m_flag;

//template<> std::shared_ptr<ProxyInt>    Singleton<ProxyInt>::m_instance    = nullptr;
//template<> std::shared_ptr<ProxyFloat>  Singleton<ProxyFloat>::m_instance  = nullptr;
//template<> std::shared_ptr<ProxyDouble> Singleton<ProxyDouble>::m_instance = nullptr;
