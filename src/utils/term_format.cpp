#include "utils/term_format.hpp"

namespace fmt {

// [0 - normal
// [1 - bold

const char* red       = "\033[0;31m";
const char* green     = "\033[0;32m";
const char* orange    = "\033[0;33m";
const char* blue      = "\033[0;34m";

const char* red_bg    = "\033[0;41m";
const char* green_bg  = "\033[0;42m";
const char* orange_bg = "\033[0;43m";
const char* blue_bg   = "\033[0;44m";

const char* reset = "\033[0m";
const char* clear = "\033[2J\033[1;1H";

// TODO: helper wrappers
// e.g.:
// ostream& wrap(format, ostream&) {} // or use tag dispathing...

} // namespace
