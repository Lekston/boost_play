#include "backtrace.h"

#if (defined(__linux) || defined(__linux__))
  #include <cxxabi.h>
  #include <execinfo.h>
#endif // linux

#include <cstring>
#include <iomanip>
#include <iostream>

#include <sstream>
#include <vector>

// instrument-functions dependencies
#include <cstdio>

extern "C" {
#include <dlfcn.h>
#include <pthread.h>
#include <sys/types.h>
}

#include <boost/current_function.hpp>

#include "globals.h"
#include "multithread_stack.h"
#include "utils/term_format.hpp"

extern "C" {
#define UNW_LOCAL_ONLY
#include <libunwind.h>
}

namespace {

template<unsigned int N>
std::ostream& hex(std::ostream& out) {
    return out << std::hex << std::setw(N) << std::setfill('0');
}

} // anonymous ns

constexpr int BT_STACK_SIZE = 20;

std::string sys_backtrace()
{
#if (defined(__linux) || defined(__linux__))

    void* buffer[BT_STACK_SIZE];
    char** strings;

    int bt = backtrace(buffer, BT_STACK_SIZE);
    strings = backtrace_symbols(buffer, bt);

    if (strings != nullptr)
    {
        std::string output;

        for(int i = 0; i < bt; ++i) {
            output += std::to_string(strlen(strings[i])) + "\t" + std::string{strings[i]} + "\n";
        }
        std::free(strings);
        return output;
    }

#endif // linux
    return {};
}

std::string unw_backtrace()
{
    unw_cursor_t cursor;
    unw_context_t context;

    // initialize cursor to current frame
    unw_getcontext(&context);
    unw_init_local(&cursor, &context);

    std::stringstream ss;
    pthread_t tid = pthread_self();

    ss << hex<12> << tid;

    // unwind frames going up the frame stack
    int step = unw_step(&cursor);
    while(step > 0) {
        unw_word_t pc;
        unw_get_reg(&cursor, UNW_REG_IP, &pc);
        if (pc == 0)
            break;
        ss << " " << hex<6> << pc;

        // unw_word_t offs;
        // char symbol[256];
        // if (unw_get_proc_name(&cursor, symbol, sizeof(symbol), &offs) == 0) {
        //     char* nameptr = symbol;
        //     int status;
        //     char* demangled = abi::__cxa_demangle(symbol, nullptr, nullptr, &status);
        //     // if (status == 0)
        //     //     nameptr = demangled;
        //     // ss << "\t(" << nameptr << "+0x" << hex<4> << offs << ")" << std::endl;
        //     // std::free(demangled);

        //     if (status != 0)
        //         ss << "\nERROR IN CXA_DEMANGLE!\n";
        //     ss << "  ofs: 0x" << hex<4> << offs << "\t" << nameptr << "\n  " << demangled << std::endl;
        //     std::free(demangled);
        // } else {
        //     ss << "\t-- error: unable to obtain symbol name for this frame\n" << std::endl;
        // }

        step = unw_step(&cursor);
    }
    // handle errors
    if (step < 0)
        ss << "\t--error: unable to move to next frame\n" << std::endl;

    return ss.str();
}

// Only first full backtrace is unique, most subsequent are partly repeating
std::string fast_backtrace()
{
    // TODO conside lockless access for larger programs
    std::stringstream ss;
    std::vector<StackData> stack_copy;
    pthread_t tid = pthread_self();

    // LOCK
    pthread_mutex_lock(&glob_instr_func_mtx);
    StackImage* sim_ptr = MapTID2Stack_at(glob_multiStack_ptr, tid);
    // COPY
    size_t size = StackImage_size(sim_ptr);
    for (size_t idx=0; idx<size; idx++)
    {
        StackData* sd_ptr = StackImage_at_idx(sim_ptr, idx);
        stack_copy.push_back({sd_ptr->depth, sd_ptr->stack_ptr, sd_ptr->call_site});
    }
    // UNLOCK
    pthread_mutex_unlock(&glob_instr_func_mtx);
    // USE
    ss << hex<12> << tid;
    for (const auto& sd : stack_copy) {
        ss << " " << hex<6> << (uint64_t)(sd.call_site);
    }
    return ss.str();
}

#ifdef __GNUC__

void read_symbols()
{
    // TODO for recording on exit from the session: parse all paths (stacks) for unique addresses, translate to symbols & store
    // TODO for playback find the addresses that correspond to recorded symbols before running the app

    // const char* fn_fname = "?";     // file name
    // const char* fn_sname = "?";     // this func symbol name
    // const char* call_sname = "?";   // call site symbol name
    //
    // TODO this can be done AFTER the recording is finished
    // Dl_info info_func;
    // Dl_info info_caller;
    // if (dladdr(this_fn, &info_func)) {
    //     fn_fname = info_func.dli_fname ? info_func.dli_fname : "?";
    //     fn_sname = info_func.dli_sname ? info_func.dli_sname : "?";
    // }
    // if (dladdr(call_site, &info_caller)) {
    //     call_sname = info_caller.dli_sname ? info_caller.dli_sname : "?";
    // }

    //std::cout << (is_entry ? "e" : "x");

    // printf("%s %03d tid: %0lx  fn: %p  caller: %p  [%s] %s\n",
    //         prefix, _depth, tid, this_fn, call_site, fn_sname, call_sname);

    // char buf[256];
    // snprintf(buf, sizeof(buf), "%s %03d fn: %p,\tcaller: %p\t[%s] %s\n",
    //         prefix _depth, this_fn, call_site, fname, sname);
}

/******************************************************************************/
// NOTE: __cyg_profile_func must not call c++ functions before 'main' is reached
/******************************************************************************/

enum ProfFuncType {
    Entry = 0,
    Exit  = 1,
};

extern "C" void _profile_func_impl (void *this_fn, void *call_site, int type)
{
    bool is_entry = (type == ProfFuncType::Entry);

    const char* prefix = is_entry ? "ENT" : "EXT";

    /* OPTIONS
     * 1. Faster search in TID_map:
     *   a. (maybe check atomic bool) if TID exists get stackMonitor ptr
     *   b. if TID does not exist: lock, change atomic, update TID_map, unlock
     * 2. incr./decr. TID stack ctr (no risk of ABA: only single thread uses each stack)
     */

    pthread_t tid = pthread_self();

    // c style atomic flag and normal c++ objects within??
    if (glob_within_main)
    {
        // TODO full C++ works here (but all must be noexcept)

        // TODO conside lockless access for larger programs
        bool stack_tracking_lost = false;
        // LOCK MTX
        pthread_mutex_lock(&glob_instr_func_mtx);

        // get or create
        StackImage* sim_ptr = MapTID2Stack_at(glob_multiStack_ptr, tid);

        if (is_entry) {
            StackData sdata;
            sdata.depth = StackImage_size(sim_ptr);
            sdata.stack_ptr = this_fn;
            sdata.call_site = call_site;
            StackImage_push_back(sim_ptr, &sdata);
        } else {
            StackData* sd_ptr = StackImage_peek_back(sim_ptr);
            if (sd_ptr->stack_ptr != this_fn) {
                stack_tracking_lost = true;
            }
            StackImage_pop_back(sim_ptr);
        }
        // UNLOCK MTX
        pthread_mutex_unlock(&glob_instr_func_mtx);

        if (stack_tracking_lost) {
            printf("\nStack tracking FAILED for TID: %ld\n", tid);
            // TODO mark this error in the Thread's StackImage
        }
    } else {
        printf("%s %s\n", prefix, "XXX");
        return;
    }
}

extern "C" void __cyg_profile_func_enter(void *this_fn, void *call_site)
{
    _profile_func_impl(this_fn, call_site, ProfFuncType::Entry);
}

extern "C" void __cyg_profile_func_exit (void *this_fn, void *call_site)
{
    _profile_func_impl(this_fn, call_site, ProfFuncType::Exit);
}

#endif // __GNUC__