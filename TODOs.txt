/* RecPlayEngine TODOs:
 *
 * Recorder:
 *  -> minimum example of test which writes to a file:
 *      * single instance objects only
 *      * unit tests (simply record input & output from some algorithm)
 *  -> keep a list of all Record points (built at run-time)
 *  ->
 *  -> limitations:
 *      * support for multiple objects of the same type require UUIDs
 *
 * NOTE:
 * THE WAY I WRITE IT SHOULD MAKE READING IT BACK AS EASY AS POSSIBLE
 *
 * Playback:
 *  -> _pending_requests queue?
 *  -> playback worker - single thread that synchronizes all other
 */

/*********************************** APM ************************************/
List of all inputs required to Playback APM:

- EKF iface
- EEPROM iface (or just let it read the file)
- Mavlink (at least on Chan1) -> GCS_MAVLINK_Plane

/* General TODOs:
 * 1. clean-up
 * 2. minimal demo:
 *      a. use fast_backtrace as "path" for variable recording
        b. replay on the same build (addresses of symbols are constant)
 * 3. Allow replay on new builds:
    - on exit from the recording session: parse all paths (stacks) for unique addresses, translate to symbols & store
    - on entry to replay session: find updated addresses by their symbols
 * 4. Handling thread IDs...
 * 5. main.cpp -> tests.cpp (gtest)
 *
 * ISSUES:
 * - gettid vs pthread_self
 * - blocking inputs
 * - clock-dependent
 *
 * OBSERVATIONS:
 * LIBUNWIND is only slightly slower (assuming symbols are not read), yet
 * it also may have NO PENAULTY in case where fast_backtrace performance
 * would be deteriorated by high numbers of function calls (and locks!)
 * in multithreaded environment. (Unless, lockless implementation is possible)
 */