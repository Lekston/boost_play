#pragma once

#include <string>

#include "utils/stack_data.h"

std::string sys_backtrace();

std::string unw_backtrace();

std::string fast_backtrace();

#ifdef __GNUC__

extern "C" void __cyg_profile_func_enter(void *this_fn, void *call_site) __attribute__((no_instrument_function));
extern "C" void __cyg_profile_func_exit (void *this_fn, void *call_site) __attribute__((no_instrument_function));
extern "C" void _profile_func_impl (void *this_fn, void *call_site, int type) __attribute__((no_instrument_function));

#endif // __GNUC__
