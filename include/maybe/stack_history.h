#pragma once

// C wrapper around std::vector<struct StackData> object according to:
// www.teddy.ch/c++_library_in_c/

#ifdef __cplusplus
extern "C" {
#endif

#include "stddef.h"
#include "stdint.h"

struct StackData {
    int   depth;
    void* stack_ptr;
    void* call_site; // prev context (prev stack_ptr with offset)
};

// stack trace vector (one per thread, grows throughout the program)

// StackHistory (all changes from the beginning of the program)

struct StackHistory;

StackHistory* StackHistory_new();

void StackHistory_delete(StackHistory* obj);

size_t StackHistory_size(StackHistory* obj);

void StackHistory_reserve(StackHistory* obj, size_t size);

void StackHistory_push_back(StackHistory* obj, StackData* data);

StackData* StackHistory_at_ix(StackHistory* obj, size_t idx);

// TODO
// implement above functions by adding corresponding calls
// to the methods of std::vector<struct StackData>

#ifdef __cplusplus
}
#endif