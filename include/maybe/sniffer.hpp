#pragma once

#include <deque>
#include <tuple>
#include <string>
#include <boost/variant.hpp>

namespace nav {
    struct Cartesian {
        double x;
        double y;
    };
}

class Sniffer
{
public:
    Sniffer()
    {}

    enum Layer {
        L0 = 0,
        L1 = 1,
        L2 = 2
    };

    template<typename T>
    void push(T& data, enum Layer layer)
    {
        _data.emplace_back( layer, data.serialize() );
    }

private:

    std::deque< std::pair<int, std::string> > _data;


    // NOTE:
    // perf penaulty -> even the fastest backtrace is locking on every single func call

    // use ONE queue per thread and a synchronizer/writer thread to merge
};