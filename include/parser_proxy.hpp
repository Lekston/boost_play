#pragma once


#include <string>
#include <sstream>

#include <boost/current_function.hpp>

#include "utils/singleton.hpp"

#include "boost_base64.hpp"
#include "boost_serialization.hpp"
#include "parser_engine.hpp"

#define USE_RECORDER_MACROS
#ifdef USE_RECORDER_MACROS
#define RECORDERS_REC_PLAY(x, y) \
        Recorders::RecPlayTypeProxy<decltype(x)>::get_instance()->rec_play((x),(y))

#define RECORDERS_REC_CHECK(x, y) \
        Recorders::RecPlayTypeProxy<decltype(x)>::get_instance()->rec_check((x),(y))

#define RECORDERS_OPT_REC_CHECK(x, y) \
        Recorders::RecPlayTypeProxy<decltype(x)>::get_instance()->optional_rec_check((x),(y)))

#define VAR_NAME(x) "F_" ##x
#define VAR_PATH(x) (std::string(BOOST_CURRENT_FUNCTION) + "|" + VAR_NAME(x))

#define SIMPLE_REC_PLAY(x)          RECORDERS_REC_PLAY((x),VAR_NAME((x)))
#define SIMPLE_REC_CHECK(x)         RECORDERS_REC_CHECK((x),VAR_NAME((x)))
#define SIMPLE_OPT_REC_CHECK(x)     RECORDERS_OPT_REC_CHECK((x),VAR_NAME((x)))
#else

#define RECORDERS_REC_PLAY(x, y)
#define RECORDERS_REC_CHECK(x, y)
#define RECORDERS_OPT_REC_CHECK(x, y)

#define SIMPLE_REC_PLAY(x)
#define SIMPLE_REC_CHECK(x)
#define SIMPLE_OPT_REC_CHECK(x)
#endif // USE_RECORDER_MACROS

// NOTE:
// Each recorded type requires:
// - a separe Proxy specialization (with static singleton members defined)
// - save/load methods for boost serialize (non-intrusive if possible)
// Objects that will be used as conditions also require
// - comparison operators == and !=

namespace Recorders {

// TODO no-instruments-functions for the RecPlay objects

template<typename T>
class RecPlayTypeProxy : public Singleton<RecPlayTypeProxy<T>> {
public:
    RecPlayTypeProxy()
     : _engine{  *RecPlayEngine::get_instance() }
     , _type_str{ typeid(T).name() }
    {
        // TODO callback to notify? / push updates during replay?
        _engine.register_proxy(_type_str, nullptr /* callback */);
    }

    void rec_play(T& obj, std::string path) {
        if (!_rec_step(obj, path))
            _play_step(obj, path);
    }

    void rec_check(T& obj, std::string path) {
        if (!_rec_step(obj, path))
            _check_step(obj, path);
    }

    void optional_rec_check(T& obj, std::string path) {
        if (_engine.is_scheduled_observation(path) && _rec_step(obj, path))
            return;
        else
            _check_step(obj, path);
    }

private:
    bool _rec_step(T& obj, std::string path) {
        if (_engine.is_accepting(path)) {
            std::string o_str = convert(obj);
            _engine.record(_type_str, path, o_str);
            return true;
        }
        return false;
    }

    void _play_step(T& obj, std::string path) {
        if (_engine.is_publishing(path)) {
            std::string o_str = _engine.get(_type_str, path);
            T obj_rec = convert(o_str);
            obj = obj_rec;
        }
    }

    void _check_step(T& obj, std::string path) {
        if (_engine.is_publishing(path)) {
            std::string rec_obj_str = _engine.get(_type_str, path);
            // object based
            // T obj_rec = convert(rec_obj_str);
            // _engine.register_test_result(path, obj != obj_rec);
            // string based
            std::string obj_str = convert(obj);
            _engine.register_test_result(path, obj_str != rec_obj_str);
        }
    }

    std::string convert(T& obj) {
        //  TODO if T is a primitive type just read/write as binary & encode
        // otherwise:
        std::stringstream ss;
        {
            BoostOBinArchive trace_archive{ss};

            trace_archive & obj;
        }
        auto enc = base64::encode_str(ss.str());

        return enc;
    }

    T convert(std::string& dec_in) {
        T obj;
        // DECODE
        auto out = base64::decode_str(dec_in);

        std::stringstream ss_out{out};
        {
            BoostIBinArchive trace_archive{ss_out};
            trace_archive & obj;
        }
        return obj;
    }

    RecPlayEngine& _engine;
    const std::string _type_str;
};

} // namespace Recorders

