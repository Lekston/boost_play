#pragma once

#include <cstdint>
#include <string>

enum EntryType {
    Input = 0,
    Condition = 1
};

// NOTE:
// using strings for types allow flexible handling of user defined classes
struct LogEntry {
	uint64_t stamp;
    EntryType entryType;

    // object is serialized by boost::serialize to bin and encoded to base64 string
	std::string objType;
	std::string objBase64;

    // NOTE: object path is used as map key so this is redundant
	// std::string objPath; // (e.g.: Vehicle::set_state);
};
