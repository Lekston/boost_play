#pragma once

#include <mutex>
#include <memory>
#include <string>


template<typename T>
class Singleton {
protected:
    Singleton() = default;
    virtual ~Singleton() = default;

public:
    static std::shared_ptr<T> get_instance() {

        std::call_once( m_flag, create_instance );

        return m_instance;
    }

protected:
    static void create_instance() {
        m_instance = std::make_shared<T>();
    }

    static std::once_flag m_flag;
    static std::shared_ptr<T> m_instance;
};

// Explicit instantiation declarations
template<typename T>
std::once_flag Singleton<T>::m_flag;

template<typename T>
std::shared_ptr<T> Singleton<T>::m_instance;
