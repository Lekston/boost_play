#pragma once

#ifdef __cplusplus
extern "C" {
#endif

struct StackData {
    int   depth;
    void* stack_ptr;
    void* call_site; // prev context (prev stack_ptr with offset)
};

#ifdef __cplusplus
}
#endif