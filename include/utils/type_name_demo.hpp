#pragma once

#include <iostream>

#include "utils/type_name.hpp"
#include "sitl.hpp"

void print_types();

struct TestTypeStruct {
    void foo() {}
    void bar(int, int) {}

    double data;
};

template<typename T>
struct TestTypeTempl
{
    T func() {
        return data;
    }

    T data;
};

void print_types()
{
    std::cout << "Types:\t"      << std::endl;
    std::cout << "uint16_t:\t"   << typeid(uint16_t).name() << std::endl;
    std::cout << "int16_t:\t"    << typeid(int16_t).name() << std::endl;
    std::cout << "uint32_t:\t"   << typeid(uint32_t).name() << std::endl;
    std::cout << "int32_t:\t"    << typeid(int32_t).name() << std::endl;
    std::cout << "float:\t"      << typeid(float).name()   << std::endl;
    std::cout << "double:\t"     << typeid(double).name()  << std::endl;
    std::cout << "Location:\t"   << typeid(SITL::Location).name()  << std::endl;
    std::cout << "Location_options:\t"   << typeid(SITL::Location_Option_Flags).name()  << std::endl;

    std::cout << "TestClass:\t" << typeid(&TestTypeStruct::data).name() << std::endl;
    std::cout << "TestClass:\t" << typeid(&TestTypeStruct::foo).name() << std::endl;
    std::cout << "TestClass:\t" << typeid(&TestTypeStruct::bar).name() << std::endl;
    std::cout << "TestClass:\t" << type_name<TestTypeStruct>() << std::endl;

    std::cout << "TestTempl:\t" << typeid(&TestTypeTempl<double>::data).name() << std::endl;
    std::cout << "TestTempl:\t" << typeid(&TestTypeTempl<double>::func).name() << std::endl;
}