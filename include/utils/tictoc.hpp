#pragma once

#include <chrono>
#include <string>

class TicToc {
public:
    TicToc()
    {
        tic();
    }

    void tic() {
        _tic = std::chrono::steady_clock::now();
    }

    std::string toc() {
        auto toc = std::chrono::steady_clock::now();
        auto dur = toc - _tic;
        auto ms = std::chrono::duration_cast<std::chrono::microseconds>(dur);
        return std::to_string( 0.001 * ms.count() ) + "ms";
    }
private:

    std::chrono::steady_clock::time_point _tic;
};