#pragma once
// source
// www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html
// \033 is same as 0x1b

namespace fmt {

// [0 - normal
// [1 - bold

extern const char* red;
extern const char* green;
extern const char* orange;
extern const char* blue;

extern const char* red_bg;
extern const char* green_bg;
extern const char* orange_bg;
extern const char* blue_bg;

extern const char* reset;
extern const char* clear;

// TODO: helper wrappers
// e.g.:
// ostream& wrap(format, ostream&) {} // or use tag dispathing...

} // namespace
