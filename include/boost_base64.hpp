#pragma once

#include <boost/archive/iterators/transform_width.hpp> 
#include <boost/archive/iterators/base64_from_binary.hpp>
#include <boost/archive/iterators/binary_from_base64.hpp>

#include "bytes.hpp"

#include <iterator>

//#define DEBUG_BOOST_BASE64
#ifdef DEBUG_BOOST_BASE64
  #include <iostream>
#endif // DEBUG_BOOST_BASE64

namespace b_arch_iter = boost::archive::iterators;

typedef b_arch_iter::transform_width<std::string::const_iterator, 6, 8>   encode_6bit_t;
typedef b_arch_iter::base64_from_binary<encode_6bit_t>                    base64_t;

typedef b_arch_iter::binary_from_base64<std::string::const_iterator>    decode_6bit_t;
typedef b_arch_iter::transform_width<decode_6bit_t, 8, 6>               binary_t;

// T = SITL::sitl_fdm

namespace base64 {

size_t standarize_padding(std::string &str);
std::string encode_str(const std::string& str);
std::string decode_str(std::string& str);

template<class T>
std::string encode_obj(const T& obj)
{
    auto tmp = const_cast<T&>(obj);
    // TODO
    // Bytes<> should use SFINAE to allow const/read-only objects as inputs
    Bytes<T, uint8_t> bytes{tmp};

    // START ENCODE
    std::string input{bytes.begin(), bytes.end()};
    std::string enc{base64_t{input.begin()}, base64_t{input.end()}};
  #ifdef DEBUG_BOOST_BASE64
    std::cout << "size: \t" << bytes.size() << std::endl;
    std::cout << "begin:\t" << (uint64_t)(const uint8_t*)(bytes.begin()) << std::endl;
    std::cout << "end:  \t" << (uint64_t)(const uint8_t*)(bytes.end()) << std::endl;
    std::cout << "Before encoding" << std::hex << std::endl;
    std::copy(bytes.begin(), bytes.end(), std::ostream_iterator<int>(std::cout, " "));
    // for(const auto x : bytes)
    // {
    //     std::cout << std::hex << (int)(x) << " ";
    // }
    std::cout << std::dec << std::endl;
    std::cout << "input string len: " << input.size() << std::endl;
    std::cout << "BASE64 len: " << enc.size() << "\n"  << enc << std::endl;
  #endif // DEBUG_BOOST_BASE64

    return enc;
}

template<class T>
T decode_obj(std::string& enc)
{
    size_t pad_chars = standarize_padding(enc);

    // DECODE
    std::string out{ binary_t{enc.begin()}, 
                     binary_t{enc.end()} };
    out.erase(out.end() - pad_chars, out.end());

    T obj;
    Bytes<T, uint8_t> bytes{obj};
    std::copy(out.begin(), out.end(), bytes.begin());

  #ifdef DEBUG_BOOST_BASE64
    std::cout << "BASE64 len: " << enc.size() << "\n"  << enc << std::endl;
    std::cout << "After decoding" << std::endl;
    std::cout << "output string len: " << out.size() << std::endl;

    for (const auto& x : out)
    {
        std::cout << std::hex << (int)((unsigned char)x) << " ";
    }
    std::cout << std::dec << std::endl;
  #endif // DEBUG_BOOST_BASE64

    return obj;
}

} // namespace base64