#pragma once

#include <memory>
#include <string>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>

#include <boost/archive/binary_iarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>

#include "sitl.hpp"
#include "utils/type_name.hpp"


using BoostOBinArchive = boost::archive::binary_oarchive;
using BoostOBinArchive_Ptr = std::shared_ptr<BoostOBinArchive>;

using BoostIBinArchive = boost::archive::binary_iarchive;
using BoostIBinArchive_Ptr = std::shared_ptr<BoostIBinArchive>;

using BoostOTxtArchive = boost::archive::text_oarchive;
using BoostOTxtArchive_Ptr = std::shared_ptr<BoostOTxtArchive>;

using BoostITxtArchive = boost::archive::text_iarchive;
using BoostITxtArchive_Ptr = std::shared_ptr<BoostITxtArchive>;


namespace boost {
namespace serialization {

template <class Archive>
void save(Archive& ar, const struct SITL::Location& loc, const unsigned int version)
{
    //std::string enc = encode_obj(loc);
    //ar & enc;
    ar & loc.options & loc.alt & loc.lat & loc.lng;
}

template <class Archive>
void load(Archive& ar, struct SITL::Location& loc, const unsigned int version)
{
    // std::string dec;
    // ar & dec;
    // loc = decode_obj<Location>(dec);
    ar & loc.options & loc.alt & loc.lat & loc.lng;
}

BOOST_SERIALIZATION_SPLIT_FREE(SITL::Location);

template<typename Archive>
void save(Archive& ar, const SITL::sitl_fdm& fdm, const unsigned version) {
    // TODO handle Location, Quaternion, Vector3f types
    // std::cout << "Archive Type: " << typeid(Archive).name() /*type_name<Archive>()*/ << std::endl;

    /*
    std::string str{'\n'};
    std::string enc = base64::encode_obj(fdm);

    ar & str & fdm.timestamp_us & enc;
    */

    ar  & fdm.latitude     & fdm.longitude
        & fdm.altitude     & fdm.heading
        & fdm.speedN       & fdm.speedE    & fdm.speedD;
        // & fdm.xAccel       & fdm.yAccel    & fdm.zAccel
        // & fdm.rollRate     & fdm.pitchRate & fdm.yawRate
        // & fdm.rollDeg      & fdm.pitchDeg  & fdm.yawDeg;
}

template<typename Archive>
void load(Archive& ar, SITL::sitl_fdm& fdm, const unsigned version) {
    // TODO handle Location, Quaternion, Vector3f types
    // std::cout << "Archive Type: " << type_name<Archive>() << std::endl;

    /*
    std::string str{'\n'};
    std::string encoded_fdm;
    ar & str & fdm.timestamp_us & encoded_fdm;

    fdm = base64::decode_obj<SITL::sitl_fdm>(encoded_fdm);
    */

    ar  & fdm.latitude     & fdm.longitude
        & fdm.altitude     & fdm.heading
        & fdm.speedN       & fdm.speedE    & fdm.speedD;
        // & fdm.xAccel       & fdm.yAccel    & fdm.zAccel
        // & fdm.rollRate     & fdm.pitchRate & fdm.yawRate
        // & fdm.rollDeg      & fdm.pitchDeg  & fdm.yawDeg;
}

//BOOST_SERIALIZATION_SPLIT_FREE(SITL::sitl_fdm);
template<class Archive>
inline void serialize( Archive & ar, SITL::sitl_fdm & t, const unsigned int file_version )
{
    split_free(ar, t, file_version);
}
/*
template<>
void serialize<>(boost::archive::text_oarchive& ar, SITL::sitl_fdm& fdm, const unsigned version);

template<>
void serialize<>(boost::archive::text_iarchive& ar, SITL::sitl_fdm& fdm, const unsigned version);
*/

} // namespace serialization
} // namespace boost
