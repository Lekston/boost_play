#pragma once

#include <cassert>
#include <cstdint>
#include <iterator>

/*
 * Based on: gist.github.com/jeetsukumaran/307264
 */

template<class T, typename O> // <input_type, iterable_type>
class Bytes 
{
public:
    class iterator
    {
    public:
        typedef iterator self_type;
        typedef O value_type;
        typedef std::size_t difference_type;
        typedef O& reference;
        typedef O* pointer;
        typedef std::forward_iterator_tag iterator_category;
        iterator(pointer ptr) : _ptr(ptr) { }
        self_type operator++() { self_type i = *this; _ptr++; return i; }
        reference operator*()  { return *_ptr; }
        pointer   operator->() { return _ptr;  }
        bool operator==(const self_type& rhs) { return _ptr == rhs._ptr; }
        bool operator!=(const self_type& rhs) { return _ptr != rhs._ptr; }
        //operator uint64_t() const { return reinterpret_cast<const uint64_t>(_ptr); }
        operator O*()         const { return _ptr; }
    protected:
        pointer _ptr;
    };

    class const_iterator
    {
    public:
        typedef const_iterator self_type;
        typedef O value_type;
        typedef std::size_t difference_type;
        typedef const O& reference;
        typedef const O* pointer;
        typedef std::forward_iterator_tag iterator_category;
        const_iterator(const pointer ptr) : _ptr(ptr) {}
        self_type operator++()       { self_type i = *this; _ptr++; return i; }
        reference operator*()        { return *_ptr; }
        const pointer   operator->() { return _ptr;  }
        bool operator==(const self_type& rhs)  { return _ptr == rhs._ptr; }
        bool operator!=(const self_type& rhs)  { return _ptr != rhs._ptr; }
        //operator uint64_t() const { return reinterpret_cast<const uint64_t>(_ptr); }
        operator const O*()   { return _ptr; }
    protected:
        pointer _ptr;
    };

    Bytes(T& obj)
        : _obj{ obj }
    {
        _size = sizeof(T)/sizeof(O);
    }

    std::size_t size() const
    {
        return _size;
    }

    O& operator[](std::size_t idx)
    {
        assert(idx < _size);
        return *( begin() + idx );
    }

    const O& operator[](std::size_t idx) const
    {
        assert(idx < _size);
        return *( begin() + idx );
    }

    iterator begin()
    {
        return iterator{ iterator{ reinterpret_cast<O*>(&_obj) } };
    }

    iterator end()
    {
        return iterator{ iterator{ reinterpret_cast<O*>(&_obj) + size() } };
    }

    const_iterator begin() const
    {
        return const_iterator{ const_iterator{ reinterpret_cast<O*>(&_obj) } };
    }

    const_iterator end() const
    {
        return const_iterator{ const_iterator{ reinterpret_cast<O*>(&_obj) + size() } };
    }

private:
    T&          _obj;
    std::size_t _size;
};
