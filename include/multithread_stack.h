#pragma once

// C wrapper around Cpp object according to:
// www.teddy.ch/c++_library_in_c/

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#include "utils/stack_data.h"

typedef unsigned long uMapKey;
//typedef void* uMapKey;

struct StackImage;

// map thread TID to its stack recording
struct uMapTID2Stack;

#define ATTR_NO_INSTR_FUNC __attribute__((no_instrument_function))

uMapTID2Stack* MapTID2Stack_new() ATTR_NO_INSTR_FUNC;

void MapTID2Stack_delete(uMapTID2Stack* obj) ATTR_NO_INSTR_FUNC;

size_t MapTID2Stack_size(uMapTID2Stack* obj) ATTR_NO_INSTR_FUNC;

StackImage* MapTID2Stack_at(uMapTID2Stack* obj, uMapKey key) ATTR_NO_INSTR_FUNC;

StackImage* MapTID2Stack_find(uMapTID2Stack* obj, uMapKey key) ATTR_NO_INSTR_FUNC;

uMapKey MapTID2Stack_insert(uMapTID2Stack* obj, uMapKey key, StackImage* data) ATTR_NO_INSTR_FUNC;

uMapKey MapTID2Stack_key_begin(uMapTID2Stack* obj) ATTR_NO_INSTR_FUNC;

uMapKey MapTID2Stack_key_next(uMapTID2Stack* obj, uMapKey prev_key) ATTR_NO_INSTR_FUNC;

// current stack image - stack dump tracked (per thread) for efficiency
// NOTE: as each stack is tracked (there is no need to unroll using mmap or similar)


StackImage* StackImage_new() ATTR_NO_INSTR_FUNC;

void StackImage_delete(StackImage* obj) ATTR_NO_INSTR_FUNC;

size_t StackImage_size(StackImage* obj) ATTR_NO_INSTR_FUNC;

bool StackImage_empty(StackImage* obj) ATTR_NO_INSTR_FUNC;

void StackImage_push_back(StackImage* obj, StackData* data) ATTR_NO_INSTR_FUNC;

StackData* StackImage_peek_back(StackImage* obj) ATTR_NO_INSTR_FUNC;

void StackImage_pop_back(StackImage* obj) ATTR_NO_INSTR_FUNC;

StackData* StackImage_at_idx(StackImage* obj, size_t idx) ATTR_NO_INSTR_FUNC;

#ifdef __cplusplus
}
#endif
