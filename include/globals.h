#pragma once

// WARNING: DO NOT TOUCH THIS UNLESS YOU WORK WITH THE STACK TRACING
extern "C" {

#include <pthread.h>
#include <stdbool.h>

struct uMapTID2Stack;

extern uMapTID2Stack* glob_multiStack_ptr;

extern bool glob_within_main;

extern pthread_mutex_t glob_instr_func_mtx;

}