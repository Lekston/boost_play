#pragma once

#include <cstdint>
#include <string>

#define PACKED

#include <boost/serialization/access.hpp>
#include <boost/serialization/split_member.hpp>

#include "boost_base64.hpp"

namespace SITL {

struct PACKED Location_Option_Flags {
    uint8_t relative_alt : 1;           // 1 if altitude is relative to home
    uint8_t unused1      : 1;           // unused flag (defined so that loiter_ccw uses the correct bit)
    uint8_t loiter_ccw   : 1;           // 0 if clockwise, 1 if counter clockwise
    uint8_t terrain_alt  : 1;           // this altitude is above terrain
    uint8_t origin_alt   : 1;           // this altitude is above ekf origin
    uint8_t loiter_xtrack : 1;          // 0 to crosstrack from center of waypoint, 1 to crosstrack from tangent exit location
};

struct PACKED Location {
    union {
        Location_Option_Flags flags;                    ///< options bitmask (1<<0 = relative altitude)
        uint8_t options;                                /// allows writing all flags to eeprom as one byte
    };
    // by making alt 24 bit we can make p1 in a command 16 bit,
    // allowing an accurate angle in centi-degrees. This keeps the
    // storage cost per mission item at 15 bytes, and allows mission
    // altitudes of up to +/- 83km
    int32_t alt:24;                                     ///< param 2 - Altitude in centimeters (meters * 100) see LOCATION_ALT_MAX_M
    int32_t lat;                                        ///< param 3 - Latitude * 10**7
    int32_t lng;                                        ///< param 4 - Longitude * 10**7
};


// assume packet as 8 bytes * 5 (minimal) or 8 bytes * 20 bytes
struct sitl_fdm {
    friend class ::boost::serialization::access;

    // this is the structure passed between FDM models and the main SITL code
    uint64_t timestamp_us;

    double latitude;                // degrees
    double longitude;               // degrees
    double altitude;                // MSL
    double heading;                 // degrees
    double speedN, speedE, speedD;  // m/s
    // double xAccel, yAccel, zAccel;       // m/s/s in body frame
    // double rollRate, pitchRate, yawRate; // degrees/s/s in body frame
    // double rollDeg, pitchDeg, yawDeg;    // euler angles, degrees
    Location home;

  #ifdef USE_INTRUSIVE_SERIALIZATION
    template<typename Archive>
    void save(Archive& ar, const unsigned version) const {
        // TODO handle Location, Quaternion, Vector3f types
        // std::cout << "Archive Type: " << type_name<Archive>() << std::endl;

        std::string str{'\n'};
        std::string enc = encode_obj(*this);

        ar  & str          & timestamp_us & enc;
        /*
            & latitude     & longitude
            & altitude     & heading
            & speedN       & speedE    & speedD
            & xAccel       & yAccel    & zAccel
            & rollRate     & pitchRate & yawRate
            & rollDeg      & pitchDeg  & yawDeg
        */
    }

    template<typename Archive>
    void load(Archive& ar, const unsigned version) {
        // TODO handle Location, Quaternion, Vector3f types
        // std::cout << "Archive Type: " << type_name<Archive>() << std::endl;

        std::string str{'\n'};
        std::string encoded_fdm;
        ar  & str          & timestamp_us & encoded_fdm;
        *this = decode_obj<SITL::sitl_fdm>(encoded_fdm);
        /*
            & latitude     & longitude
            & altitude     & heading
            & speedN       & speedE    & speedD
            & xAccel       & yAccel    & zAccel
            & rollRate     & pitchRate & yawRate
            & rollDeg      & pitchDeg  & yawDeg
        */
    }

    template<class Archive>
    void serialize(Archive & ar, const unsigned int file_version){
        ::boost::serialization::split_member(ar, *this, file_version);
    }
  #endif // USE_INTRUSIVE_SERIALIZATION
};

} // namespace