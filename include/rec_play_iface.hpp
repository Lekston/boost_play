#pragma once

#include <set>
#include <string>

namespace Recorders {

class RecorderIface {
public:

    RecorderIface() = default;
    virtual ~RecorderIface() = default;

    virtual bool is_accepting(std::string& path)             const = 0;
    virtual bool is_scheduled_observation(std::string& path) const = 0;
    virtual void record(const std::string& type,
                        const std::string& path,
                        const std::string& obj) = 0;
};

class PlayerIface {
public:
    PlayerIface() = default;
    virtual ~PlayerIface() = default;

    virtual bool is_publishing(std::string&) const        = 0;

    // wait for the closest record to occur (XXX how to recognize it?)
    virtual std::string get(const std::string& type,
                            const std::string& path)      = 0;

    // Create a playback stats container to store all test results
    // (esp. if run shuold continue after a particular fail)
    virtual bool register_test_result(std::string&, bool) = 0;

    // Reads from an archive and distributes updates..
    // Typically it is a simulation master, but it may need to wait for
    // some tasks to complete.
    virtual void run() = 0;

protected:

    std::set<std::string> _pending_requests;
};

} // namespace