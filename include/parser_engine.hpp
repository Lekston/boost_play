#pragma once

#include <fstream>
#include <functional>
#include <mutex>
#include <set>
#include <string>
#include <unordered_map>

#include "rec_play_iface.hpp"
#include "utils/singleton.hpp"
#include "log_entry.hpp"

#include "boost_serialization.hpp"

namespace Recorders {

class RecPlayEngine
    : public RecorderIface
    , public PlayerIface
    , public Singleton<RecPlayEngine>
{
public:
    enum class State {
        Idle        = 0,   // disable / enable all Recordings at run-time
        Recording   = 1,   // handles all recording ops
        Play_Master = 2,   // act as clock and master by pushing data to tested object
        Play_Wait   = 3,   // wait for a condition
    };

    struct RunState {
        bool is_active    {false};
        bool is_optional  {false};
        bool is_scheduled {false}; // Used for optional records
    };

    struct RecordMeta {
        RunState flags; // run-time only
        LogEntry entry; // persistent storage
    };

    using ProxyCallback = std::function<void(void)>;

    using ObjectMap   = std::unordered_map< int, std::string >;           /* UUID, class */
    using VariableMap = std::unordered_map< std::string, RecordMeta >;    /* var, record */
    using TypeMap     = std::unordered_map< std::string, ProxyCallback >; /* type, callback */

    RecPlayEngine(enum State state = State::Idle, std::string archive_path = "");

    void set_state(State state);
    enum State get_state() const { return _state;  }

    const VariableMap& get_variables() const { return _map_vars;  }
    const TypeMap& get_types() const         { return _map_types; }

    void register_proxy( const std::string& datatype, ProxyCallback cb );

    // Recorder implementation
    bool is_accepting(std::string& path) const override;
    bool is_scheduled_observation(std::string&) const override;

    void record(const std::string& type,
                const std::string& path,
                const std::string& data) override;

    // Player implementation
    bool is_publishing(std::string& path) const override;

    bool register_test_result(std::string&, bool) override;

    std::string get(const std::string& type,
                    const std::string& path)                 override;

    void run() override;

private:
    void _activate_archive();
    bool _inhibit_path(std::string& path) const;

    bool _do_exit{false};

    State                _state;

    std::string          _archive_path;

    std::ofstream        _ofstr;
    BoostOTxtArchive_Ptr _oarchive;

    std::ifstream        _ifstr;
    BoostITxtArchive_Ptr _iarchive;

    mutable std::mutex   _mtx;

    VariableMap          _map_vars;
    TypeMap              _map_types;
};

} // namespace Recorders
