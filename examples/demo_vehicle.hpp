#pragma once

#include "parser_proxy.hpp"

/******** EXAMPLE **********/

#include <chrono>
#include <thread>

template<typename T>
struct DemoType{
    T x;
    T y;
    T z;
};

struct DemoVehicle {
    int a;
    int b;
    float f;
    bool do_exit {false};

    DemoVehicle();
    static int input();
    static float input_f();
    void run();
};

extern DemoVehicle demo_veh;