#include "demo_vehicle.hpp"

using namespace std::chrono_literals;

struct Rate {
    Rate() = default;

    void sleep() {
        std::this_thread::sleep_for(100ms);
    }

    void exact() {
        // TODO:
        // 1. _started = std::chrono::steady_clock_now()
        // 2. ensure that each call returns at intervals of <period> (overlaping used time)
    }
};

DemoVehicle::DemoVehicle() {
    // Recorders::RecPlayTypeProxy<decltype(a)>::get_instance()->register_var(VAR_NAME(a));
    // Recorders::RecPlayTypeProxy<decltype(b)>::get_instance()->register_var(VAR_NAME(b));
    // Recorders::RecPlayTypeProxy<decltype(f)>::get_instance()->register_var(VAR_NAME(f));
}

int
DemoVehicle::input() {
    static int i { 0 };
    return i++;
}

float
DemoVehicle::input_f() {
    static float f { 0.0 };
    return f += 1.0;
}

void
DemoVehicle::run() {
    struct Rate rate;
    while(!do_exit) {
        rate.sleep();
        //rate.exact();

        a = input(); // inputs can't block!
        b = input(); // inputs can't block!
        f = input_f();

        Recorders::RecPlayTypeProxy<decltype(a)>::get_instance()->rec_play(a, "run::a");
        Recorders::RecPlayTypeProxy<decltype(f)>::get_instance()->rec_play(f, "run::f");

        /* SOME HEAVY COMPUTATION */
        a += 5;
        f *= 1.02;

        Recorders::RecPlayTypeProxy<decltype(a)>::get_instance()->rec_check(a, "run::a");
        Recorders::RecPlayTypeProxy<decltype(f)>::get_instance()->optional_rec_check(f, "run::f");

    }
}

DemoVehicle demo_veh;
